from django.test import TestCase, LiveServerTestCase
from django.test.client import Client
from django.http import HttpRequest
from .models import Status
from .forms import StatusForm
from django.apps import apps
from status.apps import StatusConfig
import time


class story6UnitTest(TestCase):
    def test_url_exists(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)
    
    def test_template_used(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'index.html')
    
    def test_can_create_status(self):
        new_status = Status.objects.create(status = 'first try')
        self.assertTrue(isinstance(new_status, Status))
        self.assertTrue(new_status.__str__(), new_status.status)
        available_status = Status.objects.all().count()
        self.assertEqual(available_status,1)
    
    def test_show_status(self):
        status = "first try"
        data = {'message' : status}
        post_data = Client().post('/', data)
        self.assertEqual(post_data.status_code, 200)
    
    def test_app(self):
        self.assertEqual(StatusConfig.name, 'status')
        self.assertEqual(apps.get_app_config('status').name, 'status')
    
    def test_greeting_exists(self):
        response = Client().get('/')
        response_content = response.content.decode('utf-8')
        self.assertIn("Hello, how are you?", response_content)
    
    def test_form(self):
        form_data = {'status' : "the status"}
        form = StatusForm(data= form_data)
        self.assertTrue(form.is_valid())
        request = self.client.post('/', data = form_data)
        self.assertEqual(request.status_code, 302)

        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)